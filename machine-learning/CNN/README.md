# feature/neural-network



## todo

* ロジスティクス回帰の実装
* ロジスティクス回帰の数理モデルの理解
* Denoising オートエンコーダ
* 制約付きボルツマンマシン

## 実装済み
* ~~逆誤差伝搬法の実装~~
* ~~クラスでニューラルネットワークを書く~~

## 参考url
* http://k-lab.e.ishikawa-nct.ac.jp/course/AC/07AC/07AC_Handouts.html


