import cPickle
import gzip
import numpy
import os
from neunet import NeuralNet


def load_dataset(name):
    path = os.path.join(os.path.split(__file__)[0], "..", "data", name)
    f = gzip.open(path, 'rb')
    data = cPickle.load(f)
    f.close()
    X_train, y_train = data[0]
    X_val, y_val = data[1]
    X_test, y_test = data[2]
    # X_train = X_train.reshape((-1, 1, 28, 28))
    # X_val = X_val.reshape((-1, 1, 28, 28))
    # X_test = X_test.reshape((-1, 1, 28, 28))
    # y_train = LabelBinarizer().fit_transform(y_train)
    # y_val = y_val.astype(np.uint8)
    # y_test = y_test.astype(np.uint8)
    return X_train, y_train, X_val, y_val, X_test, y_test

if __name__ == '__main__':
    X_train, y_train, X_val, y_val, X_test, y_test = load_dataset('mnist.pkl.gz')

    result = []

    path = os.path.join(os.path.split(__file__)[0], "..", "params", "best_model.pkl")
    params = cPickle.load(open(path))
    batch_size = 1
    layers_map = [['conv', 50], ['pool', 4], ['conv', 20], ['pool', 20], ['hidden', 10]]

    X_test = X_test.reshape(-1, 1, 28, 28)
    in_shape = (X_test.shape[0], 1, 28, 28)

    cnn = NeuralNet(in_shape, layers_map, params=params)
    outputs = cnn.fowardprop(X_test)
    print outputs.shape

    result = [numpy.argmax(output) for output in outputs]
    score = (result == y_test)
    print score.mean()
<<<<<<< HEAD:cnn/test_model.py
    
=======

>>>>>>> feature/CNN:cnn/src/test_model.py
