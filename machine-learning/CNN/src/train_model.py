import theano.tensor as T
import numpy
from neunet import NeuralNet
from logistic_sgd import load_data
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dataset='mnist.pkl.gz'
    datasets = load_data(dataset)
    train_set_x, train_set_y = datasets[0]
    test = T.extra_ops.to_one_hot(train_set_y, 10)
    print test

    batch_size = 1
    in_shape = (batch_size, 1, 28, 28)
    layers_map = [['conv', 50] , ['pool', 4], ['conv', 20] , ['pool', 20], ['hidden', 10]]

    cnn = NeuralNet(in_shape, layers_map)
    cnn.backprop(train_set_x, test, 0.1, 100)
    cnn.save_params('../params/best_model')

    plotarray = cnn.costs
    plt.plot(plotarray)
    plt.show()
