import numpy as np
import matplotlib.pyplot as plt
import cPickle
import gzip
from sklearn.datasets import fetch_mldata
from scipy import misc
from sklearn.preprocessing import LabelBinarizer

def load_dataset():
    f = gzip.open('mnist.pkl.gz', 'rb')
    data = cPickle.load(f)
    f.close()
    X_train, y_train = data[0]
    X_val, y_val = data[1]
    X_test, y_test = data[2]
    # X_train = X_train.reshape((-1, 1, 28, 28))
    # X_val = X_val.reshape((-1, 1, 28, 28))
    # X_test = X_test.reshape((-1, 1, 28, 28))
    # y_train = y_train.astype(np.uint8)
    # y_val = y_val.astype(np.uint8)
    # y_test = y_test.astype(np.uint8)
    return X_train, y_train, X_val, y_val, X_test, y_test

def nonlin(x,deriv=False):
	if(deriv==True):
	    return x*(1-x)
	return 1/(1+np.exp(-x))


def func(x):
    y = np.sin(x/1.0+10 )**2
    # y = np.sinc((x-1)/4.0)
    return np.array(abs(y))


X_train, y_train, X_val, y_val, X_test, y_test = load_dataset()
X = X_train
y = LabelBinarizer().fit_transform(y_train)


input_size = X.shape[1]
output_size = y.shape[1]
hidden_size_1 = 100
hidden_size_2 = 10
epsilon = 0.1

# AdaGrad
r0, r1, r2 = 0, 0, 0
plotarray = []
np.random.seed(1)

# randomly initialize our weights with mean 0
syn0 = 2*np.random.random((input_size, hidden_size_1)) - 1
syn1 = 2*np.random.random((hidden_size_1, hidden_size_2)) - 1
syn2 = 2*np.random.random((hidden_size_2, output_size)) - 1

i=0
for j in range(3):
    i=i+1
    for i in xrange(10000):
        input_data = X[i:i+5]
        output_data = y[i:i+5]
        l0 = input_data
        l1 = nonlin(np.dot(l0,syn0))
        l2 = nonlin(np.dot(l1,syn1))
        l3 = nonlin(np.dot(l2,syn2))

        # how much did we miss the target value?
        l3_error = output_data - l3

        loss = np.mean(np.abs(l3_error))

        plotarray.append(loss)



        if (i% 1000) == 0:
        	print loss

        if loss <0.0005:
        	print i
        	break

        # in what direction is the target value?
        # were we really sure? if so, don't change too much.
        l3_delta = l3_error*nonlin(l3,deriv=True)


        # how much did each l1 value contribute to the l2 error (according to the weights)?
        l2_error = l3_delta.dot(syn2.T)


        l2_delta = l2_error*nonlin(l2,deriv=True)

        l1_error = l2_delta.dot(syn1.T)

        l1_delta = l1_error * nonlin(l1,deriv=True)

        syn2 += epsilon*l2.T.dot(l3_delta)
        syn1 += epsilon*l1.T.dot(l2_delta)
        syn0 += epsilon*l0.T.dot(l1_delta)
        epsilon = epsilon

def neural_network(X):
	l0 = X
	l1 = nonlin(np.dot(l0,syn0))
	l2 = nonlin(np.dot(l1,syn1))
	l3 = nonlin(np.dot(l2,syn2))
	return l2,l3

# xarray = np.linspace(-1,1,100)[:,np.newaxis]
# out = neural_network(xarray)
# plt.scatter(out[:,0],out[:,1])
# plt.plot(X, y, 'ro')

plt.plot(plotarray)
plt.yscale('log')
plt.xscale('log')
plt.show()


result = []
ans =[]
print X_test.shape

print X_test.shape

for i in range(1000):
    # i = 4

    test = X_test[i:i+1]
    out1, out  = neural_network(test)
    # print out
    out_num = np.argmax(out)
    result.append(out_num)
    ans.append(y_test[i])
    # print y_test[i]
    # print out_num

    test = test.reshape((-1, 1, 28, 28))
    out_img = test[0][0]
    # misc.imsave('out.jpg', out_img)

result = np.array(result)
ans = np.array(ans)

print '---------------'
print (result == ans).mean()
print result == ans
