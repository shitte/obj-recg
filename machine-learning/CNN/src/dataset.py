#coding: utf-8
import matplotlib.pyplot as plt
import theano
import numpy as np
import os
import random
import seaborn as sns
import pandas as pd


def shared_dataset(data_xy, borrow=True):
    data_x, data_y = data_xy
    data_x = data_x.reshape(-1, 1, data_x.shape[1], data_x.shape[2])
    shared_x = theano.shared(np.asarray(data_x,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    shared_y = theano.shared(np.asarray(data_y,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    return shared_x, shared_y


def genCsv(length, name, flag):
    ramp = lambda x, t: x if x > t else 0
    step = lambda x, t: 1 if x > t else 0

    t = np.linspace(1, 10, length)

    if flag == 'OK':
        signal = np.asarray([ramp(x, 3) + 0.3*step(x, 2) + 2*np.random.rand() for x in t])/10
    if flag == 'NG':
        signal = np.asarray([ramp(x, 7) + 2*step(x, 10) + 1*np.random.rand() for x in t])/10

    t = t.reshape(length, 1)
    signal = signal.reshape(length, 1)

    elements = 0.3*np.random.rand(length, 4)
    outdata = np.c_[signal, elements]
    outdata = np.c_[elements, signal]
    outdata = np.c_[outdata, elements]
    path = os.path.join(os.path.split(__file__)[0], "../data", name)
    np.savetxt(path, outdata, delimiter=",")


class CsvConverter(object):
    def __init__(self, csvFilePath):
        pass


class CsvFormatter(object):
    def __init__(self):
        pass


class CsvCollector(object):
    def __init__(self, csvFilePath, length=0, timeColCut=False, normalize=True, header=None):
        dirOK = os.path.join(csvFilePath, 'OK')
        dirNG = os.path.join(csvFilePath, 'NG')
        dataSet = []
        csvs = []
        answers = []
        dammyFrame = np.zeros((1, length))

        # read csv files
        print 'loading csv file... in', csvFilePath
        for fileName in CsvCollector.find_all_files(csvFilePath):
            name, suffix = os.path.splitext(fileName)
            if suffix == ".csv":

                csv = pd.read_csv(fileName, header=header).as_matrix().T

                # csv = np.loadtxt(fileName, delimiter=",", dtype=np.float16)

                assert csv.shape[1] >= length, 'length should be shorter than {0} shape {1}'.format(fileName, csv.shape)
                if timeColCut is True:
                    csv = csv[1:, -length:]
                else:
                    csv = csv[:, -length:]

                # dammyFrame
                csv = np.r_[csv, dammyFrame]

                csv = csv.reshape(-1, csv.shape[0], csv.shape[1])
                dirName = os.path.dirname(fileName)
                if dirName == dirOK:
                    answer = [1, 0]
                elif dirName == dirNG:
                    answer = [0, 1]
                else:
                    print 'unknow dir error'
                dataSet.append([csv, answer])

        random.shuffle(dataSet)

        for data in dataSet:
            csvs.append(data[0])
            answers.append(data[1])
        print len(csvs), 'csv files loaded'
        self.data_x = np.vstack(csvs).astype(np.float16)

        if normalize is True:
            self.normalize()

        self.data_y = np.asarray(answers)
        print self.data_x.shape

    @classmethod
    def find_all_files(cls, dirPath):
        for root, dirs, files in os.walk(dirPath):
            yield root
            for file in files:
                yield os.path.join(root, file)



    def shuffle(self):
        dataSet = []
        csvs = []
        answers = []
        for data_x, data_y in zip(self.data_x, self.data_y):
            dataSet.append([data_x, data_y])

        random.shuffle(dataSet)
        for data in dataSet:
            csvs.append([data[0]])
            answers.append(data[1])
        self.data_y = np.asarray(answers)
        self.data_x = np.vstack(csvs)

        # self.normalize()

        self.data_y = np.asarray(answers)

    def normalize(self):
        for i, data in enumerate(self.data_x):
            for j, row in enumerate(data):
                maxVal = np.max(np.abs(row))

                # 0割り算を防ぐ
                if maxVal == 0:
                    maxVal = 1

                self.data_x[i, j] = row/maxVal

    def getShape(self):
        return self.data_x.shape

    def getDataX(self):
        return self.data_x

    def getDataY(self):
        return self.data_y

    def getShareDataSet(self):
        data_xy = (self.data_x, self.data_y)
        return shared_dataset(data_xy)


if __name__ == '__main__':
    length = 10
    for i in range(2):
        genCsv(length, "test/OK/{0}.csv".format(i), 'OK')
    for i in range(2):
        genCsv(length, "test/NG/{0}.csv".format(i), 'NG')

    dammys = CsvCollector('../data/test', length, timeColCut=True)
    csv = dammys.getDataX()
    print csv.shape
    f, axarr = plt.subplots(2, sharex=True)
    for i, ax in enumerate(axarr):
        sns.heatmap(csv[i], ax=ax, linewidth=0.1)
    plt.show()
