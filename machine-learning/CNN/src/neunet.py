# -*- coding: utf-8 -*-
import os
import theano.tensor as T
import theano
from theano.tensor.nnet import conv
from theano.tensor.signal import pool
import numpy
import cPickle


class HiddenLayer(object):
    def __init__(self, rng, in_shape, out_shape, inputs=T.matrix('x'),
                 params=None, activation=T.tanh):

        if numpy.asarray(in_shape).shape[0] > 2:
            inputs = inputs.flatten(2)

        batch_size = in_shape[0]
        in_shape = numpy.prod(in_shape[1:4])
        out_shape = numpy.prod(out_shape[1:4])

        self.inputs = inputs

        if params is None:
            W_values = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (in_shape + out_shape)),
                    high=numpy.sqrt(6. / (in_shape + out_shape)),
                    size=(in_shape, out_shape)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4
            W = theano.shared(value=W_values, name='W', borrow=True)

            b_values = numpy.zeros((out_shape,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)
        else:
            W, b = params

        self.W = W
        self.b = b

        lin_outputs = T.dot(inputs, self.W) + self.b

        self.outputs = (
            lin_outputs if activation is None
            else activation(lin_outputs)
        )
        self.params = [self.W, self.b]
        self.out_shape = (batch_size, out_shape)
        self.in_shape = (batch_size, in_shape)


class ConvLayer(object):
    def __init__(self, rng, filter_shape, in_shape,
                 inputs=T.tensor4('x'), params=None):

        assert in_shape[1] == filter_shape[1]
        self.inputs = inputs
        # inputs = inputs.reshape(in_shape)

        in_shape = numpy.asarray(in_shape)
        filter_shape = numpy.asarray(filter_shape)

        fan_in = numpy.prod(filter_shape[1:])
        fan_out = filter_shape[0] * numpy.prod(filter_shape[2:])

        if params is None:
            W_bound = numpy.sqrt(6. / (fan_in + fan_out))
            W = theano.shared(
                numpy.asarray(
                    rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                    dtype=theano.config.floatX
                ),
                borrow=True
            )

            b_values = numpy.zeros(
                (filter_shape[0],),
                dtype=theano.config.floatX
            )
            b = theano.shared(value=b_values, borrow=True)
        else:
            W, b = params

        self.W = W
        self.b = b
        
        # 2次元畳み込み
        conv_out = conv.conv2d(
            input=inputs,
            filters=self.W,
            filter_shape=filter_shape,
            image_shape=in_shape
        )

        out_shape = in_shape.copy()
        out_shape[1] = filter_shape[0]
        out_shape[2:4] = in_shape[2:4] - filter_shape[2:4] + 1

        self.outputs = T.tanh(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'))
        self.params = [self.W, self.b]
        self.in_shape = tuple(in_shape)
        self.out_shape = tuple(out_shape)


class PoolLayer(object):

    def __init__(self, in_shape, inputs, poolsize=(1, 5)):
        pooled_out = pool.pool_2d(
            input=inputs,
            ds=poolsize,
            ignore_border=True
        )

        out_shape = list(in_shape)
        out_shape[2] = int(in_shape[2] / poolsize[0])
        out_shape[3] = int(in_shape[3] / poolsize[1])

        self.W = None
        self.b = None

        self.inputs = inputs
        self.outputs = pooled_out
        self.params = [self.W, self.b]
        self.out_shape = tuple(out_shape)
        self.in_shape = in_shape


class ConvNet(object):
    def __init__(self, in_shape, conv_map, inputs=T.tensor4('x'), params=None):
        layer_map, nkerns = conv_map
        rng = numpy.random.RandomState(1234)
        num_layers = len(layer_map)
        layers = [0] * num_layers
        self.params = []

        if params is None:
            params = [None]*num_layers

        for i in range(num_layers):
            if layer_map[i] == 'conv':
                filter_shape = ([nkerns[i], in_shape[1], 3, 3])
                layers[i] = ConvLayer(
                    rng=rng,
                    inputs=inputs,
                    in_shape=in_shape,
                    filter_shape=filter_shape,
                    params=params[i]
                )
            elif layer_map[i] == 'pool':
                layers[i] = PoolLayer(
                    inputs=inputs,
                    in_shape=in_shape
                )
            else:
                print 'no layer error'
        
            in_shape = layers[i].out_shape
            inputs = layers[i].outputs

        self.out_shape = layers[-1].out_shape
        self.flatten_shape = in_shape
        self.layers = layers
        self.inputs = layers[0].inputs
        self.outputs = layers[-1].outputs
        self.params = [layer.params for layer in self.layers]

    def fowardprop(self, input_x):
        y = self.outputs
        x = self.layers[0].inputs
        f = theano.function(inputs=[x], outputss=y)
        return f(input_x)


class HiddenNet(object):
    def __init__(self, in_shape, hidden_map, inputs=T.matrix('x'), params=None):
        rng = numpy.random.RandomState(1234)
        num_layers = len(hidden_map)
        layers = [0] * num_layers
        self.params = []
        if params is None:
            params = [None]*num_layers

        for i in range(num_layers):
            layers[i] = HiddenLayer(
                rng=rng,
                inputs=inputs,
                in_shape=in_shape,
                out_shape=hidden_map[i],
                params=params[i]
            )

            inputs = layers[i].outputs
            in_shape = layers[i].out_shape

        self.num_layer = num_layers
        self.layers = layers
        self.inputs = layers[0].inputs
        self.outputss = layers[-1].outputss
        self.num_layers = num_layers
        self.params = [layer.params for layer in self.layers]

    def fowardprop(self, input_x):
        x = self.inputs
        y = self.outputs
        f = theano.function(inputs=[x], outputss=y)
        return f(input_x)

    def backprop(self, train_x, train_y, learning_rate, num_updates):
        y = T.vector('y')
        costs = []
        cost = T.sum((y - self.layers[-1].outputs)**2)
        gparams = [T.grad(cost, param) for param in self.params]
        updates = [
            (param, param - learning_rate * gparam) for param, gparam
            in zip(self.params, gparams)
        ] 
        train_model = theano.function(
            inputs=[],
            outputss=cost,
            updates=updates,
            givens={
                HiddenNet.x: train_x,
                HiddenNet.y: train_y
            }
        )
        for i in range(num_updates):
            costs.append(train_model())
        self.costs = numpy.array(costs)


class NeuralNet(object):
    def __init__(self, in_shape, layers_map, params=None):

        self.layers = []
        self.in_shape = in_shape
        self.batch_size = in_shape[0]
        self.layers_map = layers_map
        layer_type, layer_fea = layers_map[0]

        len_layers = len(layers_map)

        if params is None:
            self.params = [None]*len_layers
        else:
            self.params = params

        if layer_type == 'conv':
            self.inputs = T.tensor4('x')
        elif layer_type == 'pool':
            self.inputs = T.tensor4('x')
        elif layer_type == 'hidden':
            self.inputs = T.matrix('x')

        self.__build()

        self.outputs = self.layers[-1].outputs
        self.inputs = self.layers[0].inputs
        self.params = [layer.params for layer in self.layers]

    def __build(self):
        rng = numpy.random.RandomState(1234)
        inputs = self.inputs
        in_shape = self.in_shape
        for layer_map, param in zip(self.layers_map, self.params):
            layer_type, layer_fea = layer_map
            if layer_type == 'conv':
                filter_shape = ([layer_fea, in_shape[1], 1, 5])
                layer = ConvLayer(
                    rng=rng,
                    inputs=inputs,
                    in_shape=in_shape,
                    filter_shape=filter_shape,
                    params=param
                )

            elif layer_type == 'pool':
                layer = PoolLayer(
                    inputs=inputs,
                    in_shape=in_shape
                )
            elif layer_type == 'hidden':
                layer = HiddenLayer(
                    rng=rng,
                    inputs=inputs,
                    in_shape=in_shape,
                    out_shape=(self.batch_size, layer_fea),
                    params=param
                )

            inputs = layer.outputs
            in_shape = layer.out_shape
            inputs = layer.outputs
            self.layers.append(layer)

    def fowardprop(self, input_x, layerNum=-1):
        x = self.inputs
        # y = T.argmax(self.outputs, axis=1)
        y = self.layers[layerNum].outputs
        f = theano.function(inputs=[x], outputs=y)
        return f(input_x)
        
    def backprop(self, train_x, train_y, learning_rate, num_updates):
        x = self.inputs
        y = T.matrix('y')
        batch_size = self.batch_size
        index = T.lscalar()
        costs = []
        params = []
        cost = T.mean((y - self.outputs)**2)
        for layer in self.layers:
            params += layer.params
        params = [param for param in params if param]
        grads = T.grad(cost, params)
        updates = [
            (param, param - learning_rate * grad) for param, grad
            in zip(params, grads)
        ]
        train_model = theano.function(
            inputs=[index],
            outputs=cost,
            updates=updates,
            givens=[
                (y, train_y[batch_size*index:(index+1)*batch_size]),
                (x, train_x[batch_size*index:(index+1)*batch_size])
            ]
        )
        for i in range(num_updates):
            print i
            error = train_model(i)
            costs.append(error)
        self.costs = numpy.array(costs)
        self.params = [layer.params for layer in self.layers]

    def save_params(self, name):
        path = os.path.join(os.path.split(__file__)[0], "..", "params", name)
        with open(path, 'w') as f:
            cPickle.dump(self.params, f)
        f.close()

def shared_dataset(data_x, data_y, borrow=True):
    shared_x = theano.shared(numpy.asarray(data_x,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    shared_y = theano.shared(numpy.asarray(data_y,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    return shared_x, shared_y
