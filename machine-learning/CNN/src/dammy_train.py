from dataset import CsvCollector, genCsv
from neunet import NeuralNet
import matplotlib.pyplot as plt
import seaborn as sns; sns.set_style('white')

if __name__ == '__main__':

    # generate CSV files
    samples = 1000

    ok = samples/2
    # generating csv files  ...
    length = 10
    for i in range(ok):
        genCsv(length, "dammy/train/OK/{0}.csv".format(i), 'OK')
    for i in range(samples-ok):
        genCsv(10, "dammy/train/NG/{0}.csv".format(i), 'NG')

    # find csv files
    dammys = CsvCollector('../data/dammy/train', length, timeColCut=False)
    inDataShape = dammys.getShape()

    # init network
    batch_size = 50
    in_shape = (batch_size, 1, inDataShape[1], inDataShape[2])
    layers_map = [['conv', 1], ['pool', 1],  ['hidden', 2]]
    length = 10

    cnn = NeuralNet(in_shape, layers_map)

    train_set_x, train_set_y = dammys.getShareDataSet()

    plotarray = []
    for n in range(10):
        dammys.shuffle()
        train_set_x, train_set_y = dammys.getShareDataSet()
        cnn.backprop(train_set_x, train_set_y, 0.1, samples/batch_size)
        plotarray += cnn.costs.tolist()

    cnn.save_params('../params/dammy_model.pkl')

    plt.plot(plotarray)
    plt.yscale('log')
    plt.show()
