import numpy
from dataset import CsvCollector 
import seaborn as sns
import matplotlib.pyplot as plt

if __name__ == '__main__':
    length = 56
    dammys = CsvCollector('../data/exp/test',length, timeColCut=True)
    inDataShape = dammys.getShape()

    csvs = dammys.getDataX()
    print dammys.getDataY()

    f, axarr = plt.subplots(3, sharex=True)
    axarr[0].plot(csvs[0, 10])
    axarr[0].plot(csvs[1, 10])
    axarr[0].set_title('10ch')

    axarr[1].plot(csvs[0, 16])
    axarr[1].plot(csvs[1, 16])
    axarr[1].set_title('16ch')
    axarr[2].plot(csvs[0, 17])
    axarr[2].plot(csvs[1, 17])
    axarr[2].set_title('17ch')
    plt.show()
