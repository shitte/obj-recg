from dataset import CsvCollector
from neunet import NeuralNet
import matplotlib.pyplot as plt
import seaborn as sns

if __name__ == '__main__':

    # generate CSV files
    length = 56
    dammys = CsvCollector('../data/exp/train', length, timeColCut=True)
    inDataShape = dammys.getShape()


    batch_size = 10
    in_shape = (batch_size, 1, inDataShape[1], inDataShape[2])

    layers_map = [['conv', 3], ['pool', 10], ['hidden', 2]]
    cnn = NeuralNet(in_shape, layers_map)

    plotarray = []
    for i in range(50):
        dammys.shuffle()
        train_set_x, train_set_y = dammys.getShareDataSet()
        print 'start learning', i
        cnn.backprop(train_set_x, train_set_y, 0.05, 220/batch_size)
        plotarray += cnn.costs.tolist()
    cnn.save_params('../params/process.pkl')

    plt.plot(plotarray)
    plt.yscale('log')
    sns.plt.show()
