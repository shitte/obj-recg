import cPickle
import numpy
import os
from neunet import NeuralNet
from dataset import CsvCollector 
from dataset import genCsv
import matplotlib.pyplot as plt
import seaborn as sns; sns.set_style('white')


if __name__ == '__main__':

    length = 10
    for i in range(10):
        genCsv(length, "dammy/test/OK/{0}.csv".format(i), 'OK')
    for i in range(10):
        genCsv(length, "dammy/test/NG/{0}.csv".format(i), 'NG')

    dammys = CsvCollector('../data/dammy/test',length, timeColCut=False, normalize=True)
    inDataShape = dammys.getShape()

    path = os.path.join(os.path.split(__file__)[0], "..", "params", "dammy_model.pkl")
    params = cPickle.load(open(path))
    batch_size = 1
    layers_map = [['conv', 1], ['pool', 1],  ['hidden', 2]]
    in_shape = (batch_size, 1, inDataShape[1], inDataShape[2])
    cnn = NeuralNet(in_shape, layers_map, params=params)


    csvs = dammys.getDataX()
    # for i, ax in enumerate(axarr):
    #     print csvs[0].shape
    #     sns.heatmap(csvs[i], ax=ax, linewidth=0.1, xticklabels=False)

    csvs = csvs.reshape(-1, 1, inDataShape[1], inDataShape[2])
    print csvs.shape

    outputs = []

    csv = csvs[0].reshape(-1, 1, inDataShape[1], inDataShape[2])
    output = cnn.fowardprop(csv, 1)
    print '======'
    print output.shape
    print output[0][0]
    # f, axarr = plt.subplots(3, sharex=True)
    sns.heatmap(csv[0][0],  linewidth=0.1)
    sns.plt.show()
    sns.heatmap(output[0][0], linewidth=0.1)
    sns.plt.show()
    # sns.heatmap(output[0][1],  linewidth=0.1)

    output = cnn.fowardprop(csv)
    print output
    # for csv in csvs:
    #     csv = csv.reshape(-1, 1, inDataShape[1], inDataShape[2])
    #     output = cnn.fowardprop(csv)[0].tolist()
        # if output[0]>output[1]:
        #     output = [1, 0]
        # else:
        #     output = [0, 1]

        # outputs.append(output)

        # print output

    # for answer in dammys.getDataY():
    #     print answer

    # print 'answer is'
    #
    # score = (outputs == dammys.getDataY())
    # print 'acc is', score.mean() * 100, '%'
    #
