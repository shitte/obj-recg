import cPickle
import numpy
import os
from neunet import NeuralNet
from dataset import CsvCollector 
import seaborn as sns


if __name__ == '__main__':
    length = 56
    dammys = CsvCollector('../data/exp/test',length, timeColCut=True)
    inDataShape = dammys.getShape()


    path = os.path.join(os.path.split(__file__)[0], "..", "params", "process.pkl")
    params = cPickle.load(open(path))
    batch_size = 1
    layers_map = [['conv', 3], ['pool', 10], ['hidden', 2]]

    in_shape = (batch_size, 1, inDataShape[1], inDataShape[2])
    cnn = NeuralNet(in_shape, layers_map, params=params)

    csvs = dammys.getDataX()

    csvs = csvs.reshape(-1, 1, inDataShape[1], inDataShape[2])
    print csvs.shape

    outputs = []
    csv0 = csvs[0].reshape(-1, 1, inDataShape[1], inDataShape[2])
    csv1 = csvs[1].reshape(-1, 1, inDataShape[1], inDataShape[2])
    output0 = cnn.fowardprop(csv0, 1)
    output1 = cnn.fowardprop(csv1, 1)
    print '======'
    print output1.shape
    # f, axarr = plt.subplots(3, sharex=True)
    print 'answer0 is', dammys.getDataY()[0]
    print 'answer1 is', dammys.getDataY()[1]
    diffPool = abs(output0[0, 0] - output1[0, 0])
    sns.heatmap(diffPool, linewidth=0.1)
    sns.plt.show()
    diffPool = abs(output0[0, 1] - output1[0, 1])
    sns.heatmap(diffPool, linewidth=0.1)
    sns.plt.show()
    diffPool = abs(output0[0, 2] - output1[0, 2])
    sns.heatmap(diffPool, linewidth=0.1)
    sns.plt.show()

    output = cnn.fowardprop(csv0)
    print output
    output = cnn.fowardprop(csv1)
    print output

