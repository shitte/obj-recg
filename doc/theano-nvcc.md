# nvcc導入
theanoでGPUを用いた処理が可能である(GPGPU)。
以下にGPU導入を示す。
最低限、Nvidiaのグラフィックカードが使える状態であること。
以下のサイト参照した

1. CUDA toolkitのインストール

    CUDA toollikのbinデイレクトリには、GPU codeのコンパイラである*nvcc*programがある。
    この

    ```sh
    sudo apt-get install nvidia-cuda-toolkit
    ```

    [CUDA toolkit](https://developer.nvidia.com/cuda-toolkit)


# 参考
* [Using the GPU — Theano 0.6 documentation](http://deeplearning.net/software/theano/install.html#gpu-linux)

