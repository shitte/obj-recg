# ルール
- target 
コマンドから実行するコマンドを呼び出す名前？
```
target: depend_file
    command
```
`make target`でtargetが存在しない、もしくはtargetよりも新しい依存ファイルがある場合にコマンドが実行される。

* `make`のあとにtargetがないときは最初のルールだけが実行される。
```
sample1:
    echo sample1
sample2:
    echo sample2
sample3:
    echo sample3
```
とういうMakefileを作成し, 実行すると

```
$ make
sample1
```
```
obj = $(scr:%.c=%.o)
```
マクロ中のスペースを開けると認識されなくなる
