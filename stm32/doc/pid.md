'【制御基礎講座(古典制御編)】　2016/03/03　藤井作成
'
''' <summary>
''' 【注意】ローカルのＰＣにコピーした後、本マクロを使用して下さい。
''' 【注意】本マクロにはバグがあるかもしれません。。。
'''
''' ＜＜できるところからやってみましょう＞＞
''' ①：下記の設定で臨界制動を体験しましょう。ok
'''   Kp=900  Kd = 60.0  T_dis = 0.0
''' ②：下記の設定で不足制動を体験しましょう。ok
'''   Kp=9000  Kd = 60.0  T_dis = 0.0
''' ③：下記の設定で過制動を体験しましょう。ok
'''   Kp=900  Kd = 600.0  T_dis = 0.0
''' ④：下記の設定で外乱を体験しましょう。ok
'''   Kp=900  Kd = 60.0  T_dis = 10000.0
''' ⑤：④の設定で、加速度参照値（ddX_Ref）を求める式にＩを加えて、定常偏差を無くしましょう。ok
''' ⑥：ノミナルモデル（M_n、Kt_n）と実際の値（M、Kt）の意味を理解しましょう。?
''' ⑦：加速度参照値（ddX）から、位置応答値（X_Res）を求めるまでの式を理解しましょう。?
''' ⑧：位置応答値（X_Res）から、速度応答値（dX_Res）を求める際に、
''' 　　擬似微分（ローパスフィルタと微分）を用いてます。擬似微分（ローパスフィルタと微分）の意味を理解しましょう。?
''' ⑨：擬似微分でのカットオフ周波数（Freq_cut）の周波数的な意味を理解しましょう。
''' ⑩：擬似微分（ローパスフィルタと微分）のプログラム実装部分（Velocity(LPF)）を理解しましょう。
''' ⑪：外乱項（T_dis）を定数直打ちではあく、重力項にしてみましょう。
''' ⑫：外乱項（T_dis）に摩擦項（速度×摩擦係数）も入れてみましょう。
''' ⑬：摩擦項を「速度×摩擦係数」ではなく、静止摩擦係数も含めた摩擦モデル（色々あるので、なんでもＯＫ）を入れてみましょう。
''' ⑭：下記のプログラム全体をブロック線図で書いてみましょう。
''' ⑮：入力から出力までの伝達関数を求めましょう。
''' ⑯：外乱から出力までの伝達関数を求めましょう。
''' ⑰：⑭と⑮の周波数応答特性を図示（ボード線図）しましょう。
''' ⑱：サンプリング周期（Sampling_Rate）を色々変えてみて、サンプリング周期が応答にどのような影響が出るか体験しましょう。
''' ⑲：④の設定で、加速度参照値（ddX_Ref）を求める式にＩを加えないで、
''' 　　外乱オブザーバを加えることで定常偏差を無くしましょう。
''' ⑳：（応用）ロボットモデルを２リンク以上のロボットに変更し、⑳のロボットに円軌道を指令値しましょう。
''' 　　その際に、「順運動学、逆運動学、擬似逆行列、特異点」を理解しましょう。
'''　　 ２リンクロボットの課題は別途あるので、⑳まで到達した方はご連絡下さい。
''' </summary>
''' <remarks></remarks>
